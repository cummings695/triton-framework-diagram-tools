﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StatesConverter.Model
{
	public class Transition
	{
		public string Name { get; set; }

		public string Source { get; set; }

		public string Target { get; set; }
	}
}
