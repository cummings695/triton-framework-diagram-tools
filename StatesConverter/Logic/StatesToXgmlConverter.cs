﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using StatesConverter.Model;

namespace StatesConverter.Logic
{
	public class StatesToXgmlConverter
	{
		public XElement CreateXgmlHeader()
		{
			// Create the main XML Node for the xgml document and underlying config nodes
			XElement xgmlHeader = new XElement("section", new XAttribute("name", "xgml"));

			XElement creator = new XElement("attribute", new XAttribute("key", "Creator"));
			creator.Add(new XAttribute("type", "String"));
			creator.Value = NodeConstants.XGML_CREATOR;

			XElement version = new XElement("attribute", new XAttribute("key", "Version"));
			version.Add(new XAttribute("type", "String"));
			version.Value = NodeConstants.XGML_VERSION;

			xgmlHeader.Add(creator);
			xgmlHeader.Add(version);

			return xgmlHeader;
		}


		public XElement CreateGraphHeader()
		{
			XElement graph = new XElement("section", new XAttribute("name", "graph"));

			XElement hierarchic = new XElement("attribute", new XAttribute("key", "hierarchic"));
			hierarchic.Add(new XAttribute("type", "int"));
			hierarchic.Value = NodeConstants.XGML_HIERARCHIC;

			XElement label = new XElement("attribute", new XAttribute("key", "label"));
			label.Add(new XAttribute("type", "String"));

			XElement directed = new XElement("attribute", new XAttribute("key", "directed"));
			directed.Add(new XAttribute("type", "int"));
			directed.Value = NodeConstants.XGML_DIRECTED;

			graph.Add(hierarchic);
			graph.Add(label);
			graph.Add(directed);

			return graph;
		}


		public List<XElement> ProcessState(XElement state)
		{
			List<XElement> processedState = new List<XElement>();

			return processedState;
		}


		public XElement CreateActionNode(XElement node)
		{
			XElement actionNode = new XElement("section", new XAttribute("name", "node"));

			XElement idAttr = new XElement("attribute", new XAttribute("key", "id"));
			idAttr.Add(new XAttribute("type", "int"));
			idAttr.Value = node.Attribute("id").Value;
			actionNode.Add(idAttr);

			XElement labelAttr = new XElement("attribute", new XAttribute("key", "label"));
			labelAttr.Add(new XAttribute("type", "String"));
			labelAttr.Value += node.Attribute("id").Value + Environment.NewLine;
			labelAttr.Value += node.Attribute("action").Value + Environment.NewLine;

			foreach (XAttribute attribute in node.Attributes()) {
				if ((attribute.Name != "id") &&
				    (attribute.Name != "action") &&
				    (attribute.Name != "type") &&
				    (attribute.Name != "name")) {
					labelAttr.Value += attribute.Name + ":" + attribute.Value + Environment.NewLine;
				}
			}
			actionNode.Add(labelAttr);

			XElement graphics = new XElement("section", new XAttribute("name", "graphics"));

			XElement graphicsXAttr = new XElement("attribute", new XAttribute("key", "x"));
			graphicsXAttr.Add(new XAttribute("type", "double"));
			graphicsXAttr.Value = NodeConstants.ACTION_X;
			graphics.Add(graphicsXAttr);

			XElement graphicsYAttr = new XElement("attribute", new XAttribute("key", "y"));
			graphicsYAttr.Add(new XAttribute("type", "double"));
			graphicsYAttr.Value = NodeConstants.ACTION_Y;
			graphics.Add(graphicsYAttr);

			XElement graphicsWAttr = new XElement("attribute", new XAttribute("key", "w"));
			graphicsWAttr.Add(new XAttribute("type", "double"));
			graphicsWAttr.Value = (labelAttr.Value.Length*7).ToString();
			graphics.Add(graphicsWAttr);

			XElement graphicsHAttr = new XElement("attribute", new XAttribute("key", "h"));
			graphicsHAttr.Add(new XAttribute("type", "double"));
			graphicsHAttr.Value = NodeConstants.ACTION_H;
			graphics.Add(graphicsHAttr);

			XElement graphicsShapeAttr = new XElement("attribute", new XAttribute("key", "type"));
			graphicsShapeAttr.Add(new XAttribute("type", "String"));
			graphicsShapeAttr.Value = NodeConstants.ACTION_SHAPE;
			graphics.Add(graphicsShapeAttr);

			XElement graphicsFillAttr = new XElement("attribute", new XAttribute("key", "fill"));
			graphicsFillAttr.Add(new XAttribute("type", "String"));
			graphicsFillAttr.Value = NodeConstants.ACTION_FILL;
			graphics.Add(graphicsFillAttr);

			XElement graphicsOutlineAttr = new XElement("attribute", new XAttribute("key", "hasOutline"));
			graphicsOutlineAttr.Add(new XAttribute("type", "boolean"));
			graphicsOutlineAttr.Value = NodeConstants.ACTION_FILL;
			graphics.Add(graphicsOutlineAttr);

			actionNode.Add(graphics);

			XElement labelGraphics = new XElement("section", new XAttribute("name", "LabelGraphics"));

			XElement labelGraphicsText = new XElement("attribute", new XAttribute("key", "text"));
			labelGraphicsText.Add(new XAttribute("type", "String"));
			labelGraphicsText.Value = labelAttr.Value;
			labelGraphics.Add(labelGraphicsText);

			XElement labelFontSize = new XElement("attribute", new XAttribute("key", "fontSize"));
			labelFontSize.Add(new XAttribute("type", "int"));
			labelFontSize.Value = NodeConstants.ACTION_FONT_SIZE;
			labelGraphics.Add(labelFontSize);

			XElement labelFontName = new XElement("attribute", new XAttribute("key", "fontName"));
			labelFontName.Add(new XAttribute("type", "String"));
			labelFontName.Value = NodeConstants.ACTION_FONT_NAME;
			labelGraphics.Add(labelFontName);

			XElement labelGraphicsColor = new XElement("attribute", new XAttribute("key", "color"));
			labelGraphicsColor.Add(new XAttribute("type", "String"));
			labelGraphicsColor.Value = NodeConstants.ACTION_COLOR;
			labelGraphics.Add(labelGraphicsColor);

			XElement labelGraphicsAlign = new XElement("attribute", new XAttribute("key", "align"));
			labelGraphicsAlign.Add(new XAttribute("type", "String"));
			labelGraphicsAlign.Value = NodeConstants.ACTION_ALIGN;
			labelGraphics.Add(labelGraphicsAlign);

			XElement labelGraphicsAnchor = new XElement("attribute", new XAttribute("key", "anchor"));
			labelGraphicsAnchor.Add(new XAttribute("type", "String"));
			labelGraphicsAnchor.Value = NodeConstants.ACTION_ANCHOR;
			labelGraphics.Add(labelGraphicsAnchor);


			actionNode.Add(labelGraphics);

			return actionNode;
		}


		public XElement CreatePageNode(XElement node)
		{
			XElement pageNode = new XElement("section", new XAttribute("name", "node"));

			XElement idAttr = new XElement("attribute", new XAttribute("key", "id"));
			idAttr.Add(new XAttribute("type", "int"));
			idAttr.Value = node.Attribute("id").Value;
			pageNode.Add(idAttr);

			XElement labelAttr = new XElement("attribute", new XAttribute("key", "label"));
			labelAttr.Add(new XAttribute("type", "String"));
			labelAttr.Value += node.Attribute("id").Value + Environment.NewLine;
			labelAttr.Value += node.Attribute("page").Value + Environment.NewLine;

			foreach (XAttribute attribute in node.Attributes()) {
				if ((attribute.Name != "id") &&
				    (attribute.Name != "page") &&
				    (attribute.Name != "type") &&
				    (attribute.Name != "name")) {
					labelAttr.Value += attribute.Name + ":" + attribute.Value + Environment.NewLine;
				}
			}
			pageNode.Add(labelAttr);

			XElement graphics = new XElement("section", new XAttribute("name", "graphics"));

			XElement graphicsXAttr = new XElement("attribute", new XAttribute("key", "x"));
			graphicsXAttr.Add(new XAttribute("type", "double"));
			graphicsXAttr.Value = NodeConstants.PAGE_X;
			graphics.Add(graphicsXAttr);

			XElement graphicsYAttr = new XElement("attribute", new XAttribute("key", "y"));
			graphicsYAttr.Add(new XAttribute("type", "double"));
			graphicsYAttr.Value = NodeConstants.PAGE_Y;
			graphics.Add(graphicsYAttr);

			XElement graphicsWAttr = new XElement("attribute", new XAttribute("key", "w"));
			graphicsWAttr.Add(new XAttribute("type", "double"));
			graphicsWAttr.Value = (labelAttr.Value.Length*7).ToString();
			graphics.Add(graphicsWAttr);

			XElement graphicsHAttr = new XElement("attribute", new XAttribute("key", "h"));
			graphicsHAttr.Add(new XAttribute("type", "double"));
			graphicsHAttr.Value = NodeConstants.PAGE_H;
			graphics.Add(graphicsHAttr);

			XElement graphicsShapeAttr = new XElement("attribute", new XAttribute("key", "type"));
			graphicsShapeAttr.Add(new XAttribute("type", "String"));
			graphicsShapeAttr.Value = NodeConstants.PAGE_SHAPE;
			graphics.Add(graphicsShapeAttr);

			XElement graphicsFillAttr = new XElement("attribute", new XAttribute("key", "fill"));
			graphicsFillAttr.Add(new XAttribute("type", "String"));
			graphicsFillAttr.Value = NodeConstants.PAGE_FILL;
			graphics.Add(graphicsFillAttr);

			XElement graphicsOutlineAttr = new XElement("attribute", new XAttribute("key", "hasOutline"));
			graphicsOutlineAttr.Add(new XAttribute("type", "boolean"));
			graphicsOutlineAttr.Value = NodeConstants.PAGE_FILL;
			graphics.Add(graphicsOutlineAttr);

			pageNode.Add(graphics);

			XElement labelGraphics = new XElement("section", new XAttribute("name", "LabelGraphics"));

			XElement labelGraphicsText = new XElement("attribute", new XAttribute("key", "text"));
			labelGraphicsText.Add(new XAttribute("type", "String"));
			labelGraphicsText.Value = labelAttr.Value;
			labelGraphics.Add(labelGraphicsText);

			XElement labelFontSize = new XElement("attribute", new XAttribute("key", "fontSize"));
			labelFontSize.Add(new XAttribute("type", "int"));
			labelFontSize.Value = NodeConstants.PAGE_FONT_SIZE;
			labelGraphics.Add(labelFontSize);

			XElement labelFontName = new XElement("attribute", new XAttribute("key", "fontName"));
			labelFontName.Add(new XAttribute("type", "String"));
			labelFontName.Value = NodeConstants.PAGE_FONT_NAME;
			labelGraphics.Add(labelFontName);

			XElement labelGraphicsColor = new XElement("attribute", new XAttribute("key", "color"));
			labelGraphicsColor.Add(new XAttribute("type", "String"));
			labelGraphicsColor.Value = NodeConstants.PAGE_COLOR;
			labelGraphics.Add(labelGraphicsColor);

			XElement labelGraphicsAlign = new XElement("attribute", new XAttribute("key", "align"));
			labelGraphicsAlign.Add(new XAttribute("type", "String"));
			labelGraphicsAlign.Value = NodeConstants.PAGE_ALIGN;
			labelGraphics.Add(labelGraphicsAlign);

			XElement labelGraphicsAnchor = new XElement("attribute", new XAttribute("key", "anchor"));
			labelGraphicsAnchor.Add(new XAttribute("type", "String"));
			labelGraphicsAnchor.Value = NodeConstants.PAGE_ANCHOR;
			labelGraphics.Add(labelGraphicsAnchor);


			pageNode.Add(labelGraphics);

			return pageNode;
		}


		public XElement CreateEdgeNode(Transition transition)
		{
			XElement mainNode = new XElement("section", new XAttribute("name", "edge"));

			XElement sourceAttr = new XElement("attribute", new XAttribute("key", "source"));
			sourceAttr.Add(new XAttribute("type", "int"));
			sourceAttr.Value = transition.Source;
			mainNode.Add(sourceAttr);

			XElement targetAttr = new XElement("attribute", new XAttribute("key", "target"));
			targetAttr.Add(new XAttribute("type", "int"));
			targetAttr.Value = transition.Target;
			mainNode.Add(targetAttr);

			XElement graphics = new XElement("section", new XAttribute("name", "graphics"));

			XElement graphicsFill = new XElement("attribute", new XAttribute("key", "fill"));
			graphicsFill.Add(new XAttribute("type", "String"));
			graphics.Add(graphicsFill);

			XElement graphicsArrow = new XElement("attribute", new XAttribute("key", "targetArrow"));
			graphicsArrow.Add(new XAttribute("type", "String"));
			graphicsArrow.Value = NodeConstants.EDGE_TARGET_ARROW;
			graphics.Add(graphicsArrow);

			mainNode.Add(graphics);

			XElement labelGraphics = new XElement("section", new XAttribute("name", "LabelGraphics"));

			XElement labelStyle = new XElement("attribute", new XAttribute("key", "style"));
			labelStyle.Add(new XAttribute("type", "String"));
			labelStyle.Value = NodeConstants.EDGE_STYLE;
			labelGraphics.Add(labelStyle);

			XElement labelText = new XElement("attribute", new XAttribute("key", "text"));
			labelText.Add(new XAttribute("type", "String"));
			labelText.Value = transition.Name;
			labelGraphics.Add(labelText);

			XElement labelFont = new XElement("attribute", new XAttribute("key", "fontSize"));
			labelFont.Add(new XAttribute("type", "int"));
			labelFont.Value = NodeConstants.EDGE_FONT_SIZE;
			labelGraphics.Add(labelFont);

			XElement labelFontName = new XElement("attribute", new XAttribute("key", "fontName"));
			labelFontName.Add(new XAttribute("type", "String"));
			labelFontName.Value = NodeConstants.EDGE_FONT_NAME;
			labelGraphics.Add(labelFontName);

			XElement labelModel = new XElement("attribute", new XAttribute("key", "model"));
			labelModel.Add(new XAttribute("type", "String"));
			labelModel.Value = NodeConstants.EDGE_MODEL;
			labelGraphics.Add(labelModel);

			XElement labelPosition = new XElement("attribute", new XAttribute("key", "position"));
			labelPosition.Add(new XAttribute("type", "String"));
			labelPosition.Value = NodeConstants.EDGE_POSITION;
			labelGraphics.Add(labelPosition);

			mainNode.Add(labelGraphics);

			return mainNode;
		}


		public XElement CreateEllipseNode(
			string id,
			string label)
		{
			XElement ellipseNode = new XElement("section", new XAttribute("name", "node"));

			XElement idAttr = new XElement("attribute", new XAttribute("key", "id"));
			idAttr.Add(new XAttribute("type", "int"));
			idAttr.Value = id;
			ellipseNode.Add(idAttr);

			XElement labelAttr = new XElement("attribute", new XAttribute("key", "label"));
			labelAttr.Add(new XAttribute("type", "String"));
			labelAttr.Value = label;
			ellipseNode.Add(labelAttr);

			XElement graphics = new XElement("section", new XAttribute("name", "graphics"));

			XElement graphicsXAttr = new XElement("attribute", new XAttribute("key", "x"));
			graphicsXAttr.Add(new XAttribute("type", "double"));
			graphicsXAttr.Value = NodeConstants.ELLIPSE_X;
			graphics.Add(graphicsXAttr);

			XElement graphicsYAttr = new XElement("attribute", new XAttribute("key", "y"));
			graphicsYAttr.Add(new XAttribute("type", "double"));
			graphicsYAttr.Value = NodeConstants.ELLIPSE_Y;
			graphics.Add(graphicsYAttr);

			XElement graphicsWAttr = new XElement("attribute", new XAttribute("key", "w"));
			graphicsWAttr.Add(new XAttribute("type", "double"));
			graphicsWAttr.Value = (labelAttr.Value.Length*7).ToString();
			graphics.Add(graphicsWAttr);

			XElement graphicsHAttr = new XElement("attribute", new XAttribute("key", "h"));
			graphicsHAttr.Add(new XAttribute("type", "double"));
			graphicsHAttr.Value = NodeConstants.ELLIPSE_H;
			graphics.Add(graphicsHAttr);

			XElement graphicsShapeAttr = new XElement("attribute", new XAttribute("key", "type"));
			graphicsShapeAttr.Add(new XAttribute("type", "String"));
			graphicsShapeAttr.Value = NodeConstants.ELLIPSE_SHAPE;
			graphics.Add(graphicsShapeAttr);

			XElement graphicsFillAttr = new XElement("attribute", new XAttribute("key", "fill"));
			graphicsFillAttr.Add(new XAttribute("type", "String"));
			graphicsFillAttr.Value = NodeConstants.ELLIPSE_FILL;
			graphics.Add(graphicsFillAttr);

			XElement graphicsOutlineAttr = new XElement("attribute", new XAttribute("key", "hasOutline"));
			graphicsOutlineAttr.Add(new XAttribute("type", "boolean"));
			graphicsOutlineAttr.Value = NodeConstants.ELLIPSE_HAS_OUTLINE;
			graphics.Add(graphicsOutlineAttr);

			ellipseNode.Add(graphics);

			XElement labelGraphics = new XElement("section", new XAttribute("name", "LabelGraphics"));

			XElement labelGraphicsText = new XElement("attribute", new XAttribute("key", "text"));
			labelGraphicsText.Add(new XAttribute("type", "String"));
			labelGraphicsText.Value = labelAttr.Value;
			labelGraphics.Add(labelGraphicsText);

			XElement labelFontSize = new XElement("attribute", new XAttribute("key", "fontSize"));
			labelFontSize.Add(new XAttribute("type", "int"));
			labelFontSize.Value = NodeConstants.ELLIPSE_FONT_SIZE;
			labelGraphics.Add(labelFontSize);

			XElement labelFontName = new XElement("attribute", new XAttribute("key", "fontName"));
			labelFontName.Add(new XAttribute("type", "String"));
			labelFontName.Value = NodeConstants.ELLIPSE_FONT_NAME;
			labelGraphics.Add(labelFontName);

			XElement labelGraphicsColor = new XElement("attribute", new XAttribute("key", "color"));
			labelGraphicsColor.Add(new XAttribute("type", "String"));
			labelGraphicsColor.Value = NodeConstants.ELLIPSE_COLOR;
			labelGraphics.Add(labelGraphicsColor);

			XElement labelGraphicsAlign = new XElement("attribute", new XAttribute("key", "align"));
			labelGraphicsAlign.Add(new XAttribute("type", "String"));
			labelGraphicsAlign.Value = NodeConstants.ELLIPSE_ALIGN;
			labelGraphics.Add(labelGraphicsAlign);

			XElement labelGraphicsAnchor = new XElement("attribute", new XAttribute("key", "anchor"));
			labelGraphicsAnchor.Add(new XAttribute("type", "String"));
			labelGraphicsAnchor.Value = NodeConstants.ELLIPSE_ANCHOR;
			labelGraphics.Add(labelGraphicsAnchor);


			ellipseNode.Add(labelGraphics);

			return ellipseNode;
		}

		public XElement CreateDiamondNode(
			string id,
			string label)
		{
			XElement diamondNode = new XElement("section", new XAttribute("name", "node"));

			XElement idAttr = new XElement("attribute", new XAttribute("key", "id"));
			idAttr.Add(new XAttribute("type", "int"));
			idAttr.Value = id;
			diamondNode.Add(idAttr);

			XElement labelAttr = new XElement("attribute", new XAttribute("key", "label"));
			labelAttr.Add(new XAttribute("type", "String"));
			labelAttr.Value = label;
			diamondNode.Add(labelAttr);

			XElement graphics = new XElement("section", new XAttribute("name", "graphics"));

			XElement graphicsXAttr = new XElement("attribute", new XAttribute("key", "x"));
			graphicsXAttr.Add(new XAttribute("type", "double"));
			graphicsXAttr.Value = NodeConstants.DIAMOND_X;
			graphics.Add(graphicsXAttr);

			XElement graphicsYAttr = new XElement("attribute", new XAttribute("key", "y"));
			graphicsYAttr.Add(new XAttribute("type", "double"));
			graphicsYAttr.Value = NodeConstants.DIAMOND_Y;
			graphics.Add(graphicsYAttr);

			XElement graphicsWAttr = new XElement("attribute", new XAttribute("key", "w"));
			graphicsWAttr.Add(new XAttribute("type", "double"));
			graphicsWAttr.Value = (labelAttr.Value.Length * 7).ToString();
			graphics.Add(graphicsWAttr);

			XElement graphicsHAttr = new XElement("attribute", new XAttribute("key", "h"));
			graphicsHAttr.Add(new XAttribute("type", "double"));
			graphicsHAttr.Value = NodeConstants.DIAMOND_H;
			graphics.Add(graphicsHAttr);

			XElement graphicsShapeAttr = new XElement("attribute", new XAttribute("key", "type"));
			graphicsShapeAttr.Add(new XAttribute("type", "String"));
			graphicsShapeAttr.Value = NodeConstants.DIAMOND_SHAPE;
			graphics.Add(graphicsShapeAttr);

			XElement graphicsFillAttr = new XElement("attribute", new XAttribute("key", "fill"));
			graphicsFillAttr.Add(new XAttribute("type", "String"));
			graphicsFillAttr.Value = NodeConstants.DIAMOND_FILL;
			graphics.Add(graphicsFillAttr);

			XElement graphicsOutlineAttr = new XElement("attribute", new XAttribute("key", "hasOutline"));
			graphicsOutlineAttr.Add(new XAttribute("type", "boolean"));
			graphicsOutlineAttr.Value = NodeConstants.DIAMOND_HAS_OUTLINE;
			graphics.Add(graphicsOutlineAttr);

			diamondNode.Add(graphics);

			XElement labelGraphics = new XElement("section", new XAttribute("name", "LabelGraphics"));

			XElement labelGraphicsText = new XElement("attribute", new XAttribute("key", "text"));
			labelGraphicsText.Add(new XAttribute("type", "String"));
			labelGraphicsText.Value = labelAttr.Value;
			labelGraphics.Add(labelGraphicsText);

			XElement labelFontSize = new XElement("attribute", new XAttribute("key", "fontSize"));
			labelFontSize.Add(new XAttribute("type", "int"));
			labelFontSize.Value = NodeConstants.DIAMOND_FONT_SIZE;
			labelGraphics.Add(labelFontSize);

			XElement labelFontName = new XElement("attribute", new XAttribute("key", "fontName"));
			labelFontName.Add(new XAttribute("type", "String"));
			labelFontName.Value = NodeConstants.DIAMOND_FONT_NAME;
			labelGraphics.Add(labelFontName);

			XElement labelGraphicsColor = new XElement("attribute", new XAttribute("key", "color"));
			labelGraphicsColor.Add(new XAttribute("type", "String"));
			labelGraphicsColor.Value = NodeConstants.DIAMOND_COLOR;
			labelGraphics.Add(labelGraphicsColor);

			XElement labelGraphicsAlign = new XElement("attribute", new XAttribute("key", "align"));
			labelGraphicsAlign.Add(new XAttribute("type", "String"));
			labelGraphicsAlign.Value = NodeConstants.DIAMOND_ALIGN;
			labelGraphics.Add(labelGraphicsAlign);

			XElement labelGraphicsAnchor = new XElement("attribute", new XAttribute("key", "anchor"));
			labelGraphicsAnchor.Add(new XAttribute("type", "String"));
			labelGraphicsAnchor.Value = NodeConstants.DIAMOND_ANCHOR;
			labelGraphics.Add(labelGraphicsAnchor);


			diamondNode.Add(labelGraphics);

			return diamondNode;
		}

		public XElement CreateGroupNode(string id, XElement node)
		{
			XElement groupNode = new XElement("section", new XAttribute("name", "node"));

			XElement idAttr = new XElement("attribute", new XAttribute("key", "id"));
			idAttr.Add(new XAttribute("type", "int"));
			idAttr.Value = id;
			groupNode.Add(idAttr);

			// Create the label

			XElement labelAttr = new XElement("attribute", new XAttribute("key", "label"));
			labelAttr.Add(new XAttribute("type", "String"));
			labelAttr.Value = node.Attribute("name").Value + Environment.NewLine;
			foreach (XAttribute attribute in node.Attributes())
			{
				if (attribute.Value != "name")
				{
					labelAttr.Value += attribute.Name + ":" + attribute.Value + Environment.NewLine;
				}
			}
			groupNode.Add(labelAttr);
			XElement graphics = new XElement("section", new XAttribute("name", "graphics"));

			XElement graphicsXAttr = new XElement("attribute", new XAttribute("key", "x"));
			graphicsXAttr.Add(new XAttribute("type", "double"));
			graphicsXAttr.Value = NodeConstants.GROUP_X;
			graphics.Add(graphicsXAttr);

			XElement graphicsYAttr = new XElement("attribute", new XAttribute("key", "y"));
			graphicsYAttr.Add(new XAttribute("type", "double"));
			graphicsYAttr.Value = NodeConstants.GROUP_Y;
			graphics.Add(graphicsYAttr);

			XElement graphicsWAttr = new XElement("attribute", new XAttribute("key", "w"));
			graphicsWAttr.Add(new XAttribute("type", "double"));
			graphicsWAttr.Value = (labelAttr.Value.Length * 7).ToString();
			graphics.Add(graphicsWAttr);

			XElement graphicsHAttr = new XElement("attribute", new XAttribute("key", "h"));
			graphicsHAttr.Add(new XAttribute("type", "double"));
			graphicsHAttr.Value = NodeConstants.GROUP_H;
			graphics.Add(graphicsHAttr);

			XElement graphicsShapeAttr = new XElement("attribute", new XAttribute("key", "type"));
			graphicsShapeAttr.Add(new XAttribute("type", "String"));
			graphicsShapeAttr.Value = NodeConstants.GROUP_SHAPE;
			graphics.Add(graphicsShapeAttr);

			XElement graphicsFillAttr = new XElement("attribute", new XAttribute("key", "fill"));
			graphicsFillAttr.Add(new XAttribute("type", "String"));
			graphicsFillAttr.Value = NodeConstants.GROUP_FILL;
			graphics.Add(graphicsFillAttr);

			XElement graphicsOutlineAttr = new XElement("attribute", new XAttribute("key", "hasOutline"));
			graphicsOutlineAttr.Add(new XAttribute("type", "boolean"));
			graphicsOutlineAttr.Value = NodeConstants.GROUP_HAS_OUTLINE;
			graphics.Add(graphicsOutlineAttr);

			groupNode.Add(graphics);

			XElement labelGraphics = new XElement("section", new XAttribute("name", "LabelGraphics"));

			XElement labelGraphicsText = new XElement("attribute", new XAttribute("key", "text"));
			labelGraphicsText.Add(new XAttribute("type", "String"));
			labelGraphicsText.Value = labelAttr.Value;
			labelGraphics.Add(labelGraphicsText);

			XElement labelFontSize = new XElement("attribute", new XAttribute("key", "fontSize"));
			labelFontSize.Add(new XAttribute("type", "int"));
			labelFontSize.Value = NodeConstants.GROUP_FONT_SIZE;
			labelGraphics.Add(labelFontSize);

			XElement labelFontName = new XElement("attribute", new XAttribute("key", "fontName"));
			labelFontName.Add(new XAttribute("type", "String"));
			labelFontName.Value = NodeConstants.GROUP_FONT_NAME;
			labelGraphics.Add(labelFontName);

			XElement labelGraphicsColor = new XElement("attribute", new XAttribute("key", "color"));
			labelGraphicsColor.Add(new XAttribute("type", "String"));
			labelGraphicsColor.Value = NodeConstants.GROUP_COLOR;
			labelGraphics.Add(labelGraphicsColor);

			XElement labelGraphicsAlign = new XElement("attribute", new XAttribute("key", "align"));
			labelGraphicsAlign.Add(new XAttribute("type", "String"));
			labelGraphicsAlign.Value = NodeConstants.GROUP_ALIGN;
			labelGraphics.Add(labelGraphicsAlign);

			XElement labelGraphicsAnchor = new XElement("attribute", new XAttribute("key", "anchor"));
			labelGraphicsAnchor.Add(new XAttribute("type", "String"));
			labelGraphicsAnchor.Value = NodeConstants.GROUP_ANCHOR;
			labelGraphics.Add(labelGraphicsAnchor);


			groupNode.Add(labelGraphics);

			return groupNode;
		}

		public XElement CreateOctagonNode(XElement node)
		{
			XElement octagonNode = new XElement("section", new XAttribute("name", "node"));

			XElement idAttr = new XElement("attribute", new XAttribute("key", "id"));
			idAttr.Add(new XAttribute("type", "int"));
			idAttr.Value = node.Attribute("id").Value;
			octagonNode.Add(idAttr);

			XElement labelAttr = new XElement("attribute", new XAttribute("key", "label"));
			labelAttr.Add(new XAttribute("type", "String"));
			labelAttr.Value += node.Attribute("id").Value + Environment.NewLine;

			foreach (XAttribute attribute in node.Attributes())
			{
				if ((attribute.Name != "id"))
				{
					labelAttr.Value += attribute.Name + ":" + attribute.Value + Environment.NewLine;
				}
			}
			octagonNode.Add(labelAttr);

			XElement graphics = new XElement("section", new XAttribute("name", "graphics"));

			XElement graphicsXAttr = new XElement("attribute", new XAttribute("key", "x"));
			graphicsXAttr.Add(new XAttribute("type", "double"));
			graphicsXAttr.Value = NodeConstants.OCTAGON_X;
			graphics.Add(graphicsXAttr);

			XElement graphicsYAttr = new XElement("attribute", new XAttribute("key", "y"));
			graphicsYAttr.Add(new XAttribute("type", "double"));
			graphicsYAttr.Value = NodeConstants.OCTAGON_Y;
			graphics.Add(graphicsYAttr);

			XElement graphicsWAttr = new XElement("attribute", new XAttribute("key", "w"));
			graphicsWAttr.Add(new XAttribute("type", "double"));
			graphicsWAttr.Value = (labelAttr.Value.Length * 7).ToString();
			graphics.Add(graphicsWAttr);

			XElement graphicsHAttr = new XElement("attribute", new XAttribute("key", "h"));
			graphicsHAttr.Add(new XAttribute("type", "double"));
			graphicsHAttr.Value = NodeConstants.OCTAGON_H;
			graphics.Add(graphicsHAttr);

			XElement graphicsShapeAttr = new XElement("attribute", new XAttribute("key", "type"));
			graphicsShapeAttr.Add(new XAttribute("type", "String"));
			graphicsShapeAttr.Value = NodeConstants.OCTAGON_SHAPE;
			graphics.Add(graphicsShapeAttr);

			XElement graphicsFillAttr = new XElement("attribute", new XAttribute("key", "fill"));
			graphicsFillAttr.Add(new XAttribute("type", "String"));
			graphicsFillAttr.Value = NodeConstants.OCTAGON_FILL;
			graphics.Add(graphicsFillAttr);

			XElement graphicsOutlineAttr = new XElement("attribute", new XAttribute("key", "hasOutline"));
			graphicsOutlineAttr.Add(new XAttribute("type", "boolean"));
			graphicsOutlineAttr.Value = NodeConstants.OCTAGON_FILL;
			graphics.Add(graphicsOutlineAttr);

			octagonNode.Add(graphics);

			XElement labelGraphics = new XElement("section", new XAttribute("name", "LabelGraphics"));

			XElement labelGraphicsText = new XElement("attribute", new XAttribute("key", "text"));
			labelGraphicsText.Add(new XAttribute("type", "String"));
			labelGraphicsText.Value = labelAttr.Value;
			labelGraphics.Add(labelGraphicsText);

			XElement labelFontSize = new XElement("attribute", new XAttribute("key", "fontSize"));
			labelFontSize.Add(new XAttribute("type", "int"));
			labelFontSize.Value = NodeConstants.OCTAGON_FONT_SIZE;
			labelGraphics.Add(labelFontSize);

			XElement labelFontName = new XElement("attribute", new XAttribute("key", "fontName"));
			labelFontName.Add(new XAttribute("type", "String"));
			labelFontName.Value = NodeConstants.OCTAGON_FONT_NAME;
			labelGraphics.Add(labelFontName);

			XElement labelGraphicsColor = new XElement("attribute", new XAttribute("key", "color"));
			labelGraphicsColor.Add(new XAttribute("type", "String"));
			labelGraphicsColor.Value = NodeConstants.OCTAGON_COLOR;
			labelGraphics.Add(labelGraphicsColor);

			XElement labelGraphicsAlign = new XElement("attribute", new XAttribute("key", "align"));
			labelGraphicsAlign.Add(new XAttribute("type", "String"));
			labelGraphicsAlign.Value = NodeConstants.OCTAGON_ALIGN;
			labelGraphics.Add(labelGraphicsAlign);

			XElement labelGraphicsAnchor = new XElement("attribute", new XAttribute("key", "anchor"));
			labelGraphicsAnchor.Add(new XAttribute("type", "String"));
			labelGraphicsAnchor.Value = NodeConstants.OCTAGON_ANCHOR;
			labelGraphics.Add(labelGraphicsAnchor);


			octagonNode.Add(labelGraphics);

			return octagonNode;
		}
	}
}